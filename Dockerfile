FROM docker.io/library/gcc:latest

WORKDIR .

COPY . .

RUN gcc -o logic logic_simul.c --std=c99

CMD ["bash" , "run.sh"]
