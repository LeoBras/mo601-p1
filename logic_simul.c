#define _GNU_SOURCE

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <stdlib.h>

#define LANEMAX	26
#define PORTMAX	1024
#define MAX_ITER 1024
#define LINESZ	80

#define skip_whitespace(x)	for(;(x)[0] == ' ' || (x)[0] == '\t' || (x)[0] == '\0'; (x)++)
#define next_token(x)	do {							\
				for(;(x)[0] != ' ' && (x)[0] != '\t'; (x)++);	\
				skip_whitespace(x);				\
			} while(0)

#define valid_letter(c) (c >= 'A' && c <= 'Z')
#define valid_number(c) (c >= '0' && c <= '9')

struct io {
	bool value;
	bool done;
};

enum porttype {
	NOT,
	AND,
	NAND,
	OR,
	NOR,
	XOR,
	NUMPORTS
};

struct logport {
	enum porttype type;
	int out_idx;
	int in_idx[2];
};

struct circuit {
	struct io *lanes;
	int nlanes;
	int iteration;

	struct logport *ports;
	int nports;
	int delay;
};

void validate_port(struct logport *p, int pn)
{
	if (p->type >= NUMPORTS || p->type < 0)
		error(-1, 22, "Invalid port type %d : %d\n", pn, p->type);

	if (p->out_idx < 0   || p->out_idx >= 26)
		error(-1, 22, "Invalid output in port %d : %d\n", pn, p->out_idx);

	if (p->in_idx[0] < 0 || p->in_idx[0] >= 26)
		error(-1, 22, "Invalid input1 in port %d : %d\n", pn, p->in_idx[0]);

	if (p->type != NOT && (p->in_idx[1] < 0 || p->in_idx[1] >= 26))
		error(-1, 22, "Invalid input2 in port %d : %d\n", pn, p->in_idx[1]);
}

bool _runport(enum porttype type, struct io *in1, struct io *in2, struct io *out)
{
	bool inv = false;

	switch (type) {
	case NOT:
		if (in1->done) {
			out->value = !in1->value;
			out->done = true;
		}
		break;
	case NAND:
		inv = true;
		/* Fall through*/
	case AND:
		if ((in1->done && !in1->value) ||
		    (in2->done && !in2->value) ){
			out->value = inv;
			out->done = true;
		} else if (in1->done && in2->done) {
			out->value = !inv;
			out->done = true;
		}
		break;
	case OR:
		inv = true;
		/* Fall through*/
	case NOR:
		if ((in1->done && in1->value) ||
		    (in2->done && in2->value) ){
			out->value = inv;
			out->done = true;
		} else if (in1->done && in2->done) {
			out->value = !inv;
			out->done = true;
		}
		break;
	case XOR:
		if (in1->done && in2->done) {
			out->value = in1->value ^ in2->value;
			out->done = true;
		}
		break;
	}

	return out->done;
}

bool runport(struct circuit *circ, int i)
{
	struct logport *p = &circ->ports[i];
	struct io *out, *in1, *in2;

	out = &circ->lanes[circ->iteration * LANEMAX + p->out_idx];
	if (out->done)
		return true;

	in1 = &circ->lanes[(circ->iteration - circ->delay) * LANEMAX + p->in_idx[0]];
	in2 = &circ->lanes[(circ->iteration - circ->delay) * LANEMAX + p->in_idx[1]];

	out->done = _runport(p->type, in1, in2, out);
	return out->done;
}

void run_iteration(struct circuit *circ)
{
	int done, prev = 0;

	do {
		done = 0;
		for (int i = 0; i < circ->nports; i++)
			done += runport(circ, i) ? 1 : 0;

		if (prev == done)
			error(-1, 22, "Infinite loop detected for iteration %d\n",
			      circ->iteration);

		prev = done;

		if (circ->delay != 0)
			break;
	} while (done != circ->nports);
}



void parse_line(char *line, struct circuit *circ)
{
	char *part2;
	int len;
	char c;

	skip_whitespace(line);
	/* Timeskip */
	if (line[0] == '+') {
		int num;
		sscanf(line, "+%d", &num);
		circ->iteration += num;
		if (circ->iteration >= MAX_ITER)
			error(-1, 22, "Invalid timeskip: %d in :%s:\n", num, line);
		return;
	}

	len = strnlen(line, LINESZ);
	if (len < 2)
		return;

	for (part2 = line; part2 < line + len; part2++){
		if (part2[0] == '=')
			break;
	}

	if (part2 - len >= line)
		goto err;

	next_token(part2);

	c = part2[0];
	if (valid_number(c)) { /* Input */
		if (!(valid_letter(line[0])))
			goto err;

		for (int i = 0; i < LINESZ / 2; i++) {
			struct io *lane;

			if (valid_letter(line[i])) {
			    if(!valid_number(part2[i]))
				goto err;
			} else {
			    break;
			}

			if (circ->nlanes < (line[i] - 'A'))
				circ->nlanes = line[i] - 'A';

			/* Starting values*/
			lane = &circ->lanes[circ->iteration * LANEMAX + (line[i] - 'A')];
			lane->value = (part2[i] > '0');
			lane->done = true;
		}
	} else if (valid_letter(c)){ /* Ports */
		struct logport *p = &circ->ports[circ->nports++];

		if (!valid_letter(line[0]))
			goto err;

		p->out_idx = line[0] - 'A';
		if (p->out_idx > circ->nlanes)
			circ->nlanes = p->out_idx;

		if (c == 'A')
			p->type = AND;
		else if (c == 'O')
			p->type = OR;
		else if (c == 'X')
			p->type = XOR;
		else if (c == 'N') {
			if (part2[1] == 'A')
				p->type = NAND;
			else if (part2[2] == 'R')
				p->type = NOR;
			else if (part2[2] == 'T')
				p->type = NOT;
		}

		for (int i = 0; i < 2 ; i++) {
			next_token(part2);
			c = part2[0];
			if (!valid_letter(c))
				goto err;

			p->in_idx[i] = c - 'A';

			if (p->type == NOT)
				return;
		}
	} else {
		goto err;
	}

	return;
err:
	error(-1, errno, "Invalid line: %s\n", line);
}

int parse_file(char *filename, struct circuit *circ)
{
	FILE *file;
	char line[LINESZ];

	file = fopen(filename, "r");
	if (!file)
		error(-1, errno, "File not found: %s\n", filename);

	while (fgets(line, LINESZ, file)) {
		parse_line(line, circ);
	}

	fclose(file);
}


void init_circ(struct circuit *circ, int delay)
{
	circ->iteration = 0;
	circ->nlanes = 0;
	circ->lanes = calloc(LANEMAX * (MAX_ITER + PORTMAX * delay + 1), sizeof(*circ->lanes));
	if (!circ->lanes)
		error(-1, errno, "Lane allocation failed\n");

	circ->nports = 0;
	circ->ports = malloc(PORTMAX * sizeof(*circ->ports));
	if (!circ->ports)
		error(-1, errno, "Port allocation failed\n");

	memset(circ->ports, -1, PORTMAX * sizeof(*circ->ports));
	circ->delay = delay;
}

void circ_rework(struct circuit *circ, int iter)
{
	/* Replicate last input values */
	for (int i = 0; i < LANEMAX; i++) {
		bool lastval = false;
		bool write = false;
		for (int j = 0; j <= iter; j++) {
			struct io *lane = &circ->lanes[j * LANEMAX + i];

			if (lane->done) {
				lastval = lane->value;
				write = true;
			} else if (write){
				lane->value = lastval;
				lane->done = true;
			}
		}

	}
}

void print_output(struct circuit *circ)
{
	printf("Tempo");
	for (int i = 0; i <= circ->nlanes; i++)
		printf(",%c", 'A' + i);

	putchar('\n');

	for (int i = 0; i <= circ->iteration ; i++){
		int row = i * LANEMAX;

		printf("%d", i);
		for (int j = 0; j <= circ->nlanes; j++)
			printf(",%d", circ->lanes[row + j].value);

		putchar('\n');
	}

}

bool repeated_outputs(struct circuit *circ)
{
	bool same = true;
	int cur_it = circ->iteration * LANEMAX;
	int prev_it = (circ->iteration - 1) * LANEMAX;

	for (int i = 0; i <= circ->nlanes; i++){
		if (circ->lanes[cur_it + i].value != circ->lanes[prev_it + i].value)
			same = false;
	}

	return same;
}

void initial_done(struct circuit *circ)
{
	int cur_it = circ->iteration * LANEMAX;

	for (int i = 0; i <= circ->nlanes; i++)
		circ->lanes[cur_it + i].done = true;
}

int main (int argc, char **argv)
{
	int delay, iter_max, iter_min;
	struct circuit circ;

	if (argc < 4)
		error(-1, errno, "Not enough arguments.\n"
				 " %s [delay] [circuito.hdl] [estimulos.txt]\n",
		      argv[0]);

	delay = atoi(argv[1]);

	init_circ(&circ, delay);
	parse_file(argv[2], &circ);
	parse_file(argv[3], &circ);

	for (int i = 0; i < circ.nports; i++)
		validate_port(&circ.ports[i], i);

	iter_max = circ.iteration + circ.nports * circ.delay + 1;
	iter_min = circ.iteration + circ.delay;
	circ_rework(&circ, iter_max);

	for (circ.iteration = 0; circ.iteration <= iter_max ; circ.iteration++){
		if (circ.iteration < delay) {
			initial_done(&circ);
			continue;
		}

		run_iteration(&circ);

		if (circ.iteration > iter_min && repeated_outputs(&circ))
			break;
	}

	print_output(&circ);

}
