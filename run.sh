#!/bin/bash

if ! test -f "logic" ; then
  gcc -o logic logic_simul.c --std=c99
fi

if test "$1" == "-v" ; then
  verbose=1
  out=" 2>&1 | tee"
else
  verbose=0
  out=" 2>&1 > "
fi

call_test(){
  if test "$verbose" == "1"; then
    ./logic $1 $2/circuito.hdl $2/estimulos.txt 2>&1 | tee tmpfile
  else
    ./logic $1 $2/circuito.hdl $2/estimulos.txt &> tmpfile
  fi

  out=$(diff -u $2/saida${1}.csv tmpfile)
  if test -z ${out::1}; then
    echo ok
  else
    echo nok
    echo "$out"
  fi
}

for t in test/* ; do
  echo "Testing $t"
  echo -n "Delay=0 "
  call_test 0 $t
  echo -n "Delay=1 "
  call_test 1 $t
done
